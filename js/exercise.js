$(function(){
	
	/* carousel */
	function init_exercises_carousel(carousel) {
		$('.jcarousel-control a').bind('click', function() {
			carousel.scroll(jQuery.jcarousel.intval(jQuery(this).text()));
			return false;
		});
		
		$('.j-jcarousel_control_item').on('click', function(){
			var $self = $(this);
			$('.m-jcarousel_control_item-active').toggleClass('m-jcarousel_control_item-active');
			$self.toggleClass('m-jcarousel_control_item-active');
		});	
		$('.jcarousel-prev').on('click', function(){
			if($(this).hasClass('jcarousel-prev-disabled')) return;
			var $activeSlide = $('.m-jcarousel_control_item-active');
			$activeSlide.prev().toggleClass('m-jcarousel_control_item-active');
			$activeSlide.toggleClass('m-jcarousel_control_item-active');
		});
		$('.jcarousel-next').on('click', function(){
			if($(this).hasClass('jcarousel-next-disabled')) return;
			var $activeSlide = $('.m-jcarousel_control_item-active');
			$activeSlide.next().toggleClass('m-jcarousel_control_item-active');
			$activeSlide.toggleClass('m-jcarousel_control_item-active');
		});
	}
	$("#exercises_carousel").jcarousel({
		scroll: 1,
		initCallback: init_exercises_carousel,
	});
	
	
	
	$('.j-show_whole_text').on('click', function(){
		$(this).toggleClass('m-decorative_gorizontal_divider__btn-open')
		$('.j-entry_text').toggle();
		$('.j-whole_text').toggle();
	});
	
	$('.j-start_learn_phrases').on('click', function(){
		$(this).toggleClass('m-phrases_block__start_learn-active');
		$('.j-phrases_block_show').toggle();
	});
	
	$('.j-comment_reply').on('click', function(){
		var $commentWrapper = $(this).closest('.j-comment_wrapper'),
			replyBlock = $('.j-reply_block_template').html();
		$commentWrapper.after(replyBlock);
	});
	
});