$(function () {

	var $slides = $('.j-slide'),
		$items = $('.j-item'),  
		data = {
			colors: [],
			offsets: [],
			heights: []	 
		},
        activeItem = 0;
	
	var refresh = function(){
        $slides.each(function(index, el){
			data.colors[index] = $(el).data('color');
			data.offsets[index] = $(el).offset().top;
			data.heights[index] = $(el).outerHeight();
		});	   
	}
	
	var check = function($item, index){ 		
        var itemOffset = Math.round($item.offset().top + $item.outerHeight()/2);
        for (var i=0; i<data.offsets.length; i++){
			if ((itemOffset >= data.offsets[i]) && (itemOffset <= (data.offsets[i] + data.heights[i]))){
				color = data.colors[i];
				if (color) {
                    $item.addClass('m-menu__item-light');
                } else {
                    $item.removeClass('m-menu__item-light');
                } 
                if (index == activeItem ) {
                    $items.removeClass('m-menu__item-active');   
                    $('.j-item[data-target="' + i + '"').addClass('m-menu__item-active'); 
                    activeItem = i;
                }                              
			} 	
		}         					
	};
	
	$(window).on('scroll', function(){
        refresh();
		$items.each(function(index, el){
			check($(el), index);
		});	
	}).trigger('scroll');    
    


});