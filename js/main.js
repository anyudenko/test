﻿$(function () {

    var links = $('.j-navigation').find('li');
    var slide = $('.j-slide');
    var button = $('.button');
    var mywindow = $(window);
    var htmlbody = $('html,body');




  $('.j-navigation').onePageNav();
	

    if( $('.j-slider').length ){
        $('.j-slider').superslides({
            animation: 'slide',
            play: 8000,
            pagination: false,
            inherit_width_from : '.n-block__10 .b-slider',
            inherit_height_from: '.n-block__10 .l-wrapper'
        });
    }
    $('.j-modal__video').click(function(){
        $('.b-video__block').arcticmodal();
        return false;
    });
    $('.j-reg__btn').click(function(){
        $('.j-popup__reg').arcticmodal();
        return false;
    });
    $('.b-footer-menu .title').click(function(){

        if( !$(this).hasClass('item-open') ){
            $('.b-footer-menu .title').removeClass('item-open');
            $('.b-footer-menu .title').next('ul').slideUp();
            $(this).addClass('item-open');
            $(this).next('ul').slideDown();
        }else{
            $('.b-footer-menu .title').removeClass('item-open');
            $('.b-footer-menu .title').next('ul').slideUp();
        }
        return false;
    });

    $('.b-level__item').click(function(){
        $('.b-level__item').removeClass('level-select');
        $(this).addClass('level-select');
    });

    $('.pe_heder-menu button').click(function(){
        if( !$(this).prev().hasClass('menu-open') ){
            $(this).prev().addClass('menu-open');
            $(this).next('ul').show();
        }else{
            $(this).prev().removeClass('menu-open');
            $(this).next('ul').hide();
        }
        return false;
    });
	
	$('.pe_heder-menu > ul  > li.drop-menu').click(function(){
		$(this).toggleClass('main-open');
		$(this).find('.sub-menu').slideToggle();
		return false;
	});


});



